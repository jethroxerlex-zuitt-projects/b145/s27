db.fruits.insertMany([
	{
		"name": "Apple",
		"color": "Red",
		"stock": 20,
		"price": 40,
		"onSale": true,
		"origin": ["Philippines","US"]
	},
		{
		"name": "Banana",
		"color": "Yellow",
		"stock": 15,
		"price": 20,
		"onSale": true,
		"origin": ["Philippines","Equador"]
	},
		{
		"name": "Kiwi",
		"color": "Green",
		"stock": 25,
		"price": 50,
		"onSale": true,
		"origin": ["China","US"]
	},
		{
		"name": "Mango",
		"color": "Yellow",
		"stock": 10,
		"price": 120,
		"onSale": false,
		"origin": ["Philippines","India"]
	}

	]);

//Aggregation Pipeline
// syntax:
// db.collection.aggregate([
// 	{stageA},
// 	{stageB},
// 	{stageC},
//	...
// 	]);

//Stages

	//$match - Filters the documents to pass only the documents that match the specified condition(s) to the next pipeline stage.
	//syntax:
	//{$match: {query}}

db.fruits.aggregate([{
	$match:{"onSale":true}
}])

//$count - returns a count of the number of documents at this stage of the aggregation pipeline.
//{$count: <string>}
db.fruits.aggregate([{
	$match:{"onSale":true}
},
{
	$count:"fruitCount"
}]);


//$group
	//syntax:
		// {
		// 	$group:{
		// 		_id:<expression>,//group by expression
		// 		<field1>:{<accumalator1>:<expression1>},
		// 		...
		// 	}
		// }

db.fruits.aggregate(
[
	{$match: {"onSale": true} },
	{$group: 
		{
			_id: null,
			"total": {$sum: "$stock" }
		} 
	}
]
);

//$project - reshapes each document in the stream, such as by adding a new fields or removing existing fields
	//syntax:
	//{$project:{specification(s)}}

	db.fruits.aggregate([{
	$match:{"stock":{$gt:10}}
},
{
	$project:{"_id":0}
}]);

	//sort returned documents in ascending order according to price
	//$sort - reorders the document stream by a specified sort key. only the order changes;the documents remain unchanged.


		db.fruits.aggregate([{
	$match:{"stock":{$gt:10}}
},
{
	$project:{"_id":0}
},
{
	$sort:{"price":1}//ascending order
	//$sort:{"price":-1}//descending order
}]);